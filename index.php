<?php

use Pecee\SimpleRouter\SimpleRouter;

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/routes.php';


SimpleRouter::setDefaultNamespace('\IoC\WalletBackend');
SimpleRouter::start();

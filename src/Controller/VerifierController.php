<?php

namespace Ioc\WalletBackend\Controller;

use Ioc\WalletBackend\Service\SSIKitAuditorService;
use Pecee\SimpleRouter\SimpleRouter;

class VerifierController
{
    /**
     * @var SSIKitAuditorService
     */
    protected $ssiKitService;

    public function __construct()
    {
        $this->ssiKitService = new SSIKitAuditorService();
    }

    /**
     * @return false|string
     */
    public function placeOrderAction() {

        $request = SimpleRouter::request();
        $inputHandler = $request->getInputHandler();

        $vpsJson = $inputHandler->post('vps', null);
        $basketJson = $inputHandler->post('basket', null);
        $vps = json_decode($vpsJson, true);
        $basket = json_decode($basketJson, true);

        $verificationResult = [];

        foreach($vps as $vp) {

            $result = $this->ssiKitService->verifyPresentation($vp);

            $verificationResult[$vp['id']] = json_decode($result->getContents(), true);
        }

        $orderValid = true;
        $feedback = [];
        foreach($verificationResult as $key => $value) {
            if(!$value['valid']) {
                $orderValid = false;
                $feedback[$key] = $value['policyResults'];
            }
        }

        if($orderValid) {
            // do something with the basket, save order, send confirmation mail, etc.
        }

        $response = SimpleRouter::response();
        $response->header('Content-Type: application/json; charset=utf-8');
        return json_encode(['orderValid' => $orderValid, 'errors' => $feedback]);
    }

}

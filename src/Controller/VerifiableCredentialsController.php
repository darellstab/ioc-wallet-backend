<?php

namespace Ioc\WalletBackend\Controller;

use Ioc\WalletBackend\Service\SSIKitCustodianService;
use Pecee\SimpleRouter\SimpleRouter;

class VerifiableCredentialsController
{
    /**
     * @var SSIKitCustodianService
     */
    protected $ssiKitService;

    public function __construct()
    {
        $this->ssiKitService = new SSIKitCustodianService();
    }

    public function defaultAction() {
        $response = SimpleRouter::response();

        $response->httpCode(200);
    }

    /**
     * @param $type
     * @return false|string
     */
    public function listAction($type = null) {

        $credentials = $this->ssiKitService->fetchCredentials();
        $responseAsArray = json_decode($credentials, true);
        $credentialsArray = $responseAsArray['list'];


        if($type !== null) {

            $filteredCredentials = array_filter($credentialsArray, function($item) use ($type) {
                return in_array($type, $item['type']);
            });

            $credentialsArray = $filteredCredentials;
        }

        $response = SimpleRouter::response();
        $response->header('Content-Type: application/json; charset=utf-8');
        return json_encode($credentialsArray);
    }

    /**
     * @param $alias
     * @return \Pecee\Http\Response
     */
    public function storeAction($alias) {

        $request = SimpleRouter::request();
        $inputHandler = $request->getInputHandler();

        $vc = $inputHandler->post('vc', null);

        $vcArray = json_decode($vc->getValue(), true);

        $response = SimpleRouter::response();

        try {
            $result = $this->ssiKitService->storeCredentials($alias, $vcArray);
        } catch (\Exception $exception) {
            return $response->httpCode(500);
        }

        return $response->httpCode(201);
    }


    public function presentCredentialsAction() {

        $request = SimpleRouter::request();
        $inputHandler = $request->getInputHandler();

        $vcs = $inputHandler->post('vcs', null);

        $vcsArray = json_decode($vcs->getValue(), true);
        $holderDidValue = 'did:key:z6Mkqum4YCcUzTJBbhEp1PFXBtskrdNQwhwLw1WSfL9Cq4p6';

        $mappedArray = array_map(function($item) {
            return json_encode($item);
        }, $vcsArray);

        $result = $this->ssiKitService->presentCredentials($holderDidValue, $mappedArray);

        $response = SimpleRouter::response();
        $response->header('Content-Type: application/json; charset=utf-8');
        return $result;
    }

}

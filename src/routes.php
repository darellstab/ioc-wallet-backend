<?php

use Pecee\SimpleRouter\SimpleRouter;

SimpleRouter::get('/', [\Ioc\WalletBackend\Controller\VerifiableCredentialsController::class, 'defaultAction']);
SimpleRouter::get('/credentials/list/{type?}', [\Ioc\WalletBackend\Controller\VerifiableCredentialsController::class, 'listAction']);
SimpleRouter::post('/authenticate', [\Ioc\WalletBackend\Controller\AuthenticationController::class, 'authenticateAction']);
SimpleRouter::options('/authenticate', [\Ioc\WalletBackend\Controller\AuthenticationController::class, 'corsPreflightAction']);
SimpleRouter::put('/credentials/store/{alias}', [\Ioc\WalletBackend\Controller\VerifiableCredentialsController::class, 'storeAction']);
SimpleRouter::post('/credentials/present', [\Ioc\WalletBackend\Controller\VerifiableCredentialsController::class, 'presentCredentialsAction']);
SimpleRouter::options('/credentials/present', [\Ioc\WalletBackend\Controller\AuthenticationController::class, 'corsPreflightAction']);
SimpleRouter::put('/orders/place', [\Ioc\WalletBackend\Controller\VerifierController::class, 'placeOrderAction']);
SimpleRouter::options('/orders/place', [\Ioc\WalletBackend\Controller\AuthenticationController::class, 'corsPreflightAction']);

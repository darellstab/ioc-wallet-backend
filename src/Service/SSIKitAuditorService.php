<?php

namespace Ioc\WalletBackend\Service;

use GuzzleHttp\Client;

class SSIKitAuditorService
{

    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'http://ssikit:7003']);
    }

    /**
     * @param array $vp
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function verifyPresentation(array $vp) {
        $response = $this->client->request('POST', 'v1/verify', [
            'json' => $vp
        ]);

        return $response->getBody();
    }

}

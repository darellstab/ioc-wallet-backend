<?php

namespace Ioc\WalletBackend\Service;

use GuzzleHttp\Client;

class SSIKitCustodianService
{

    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'http://ssikit:7002']);
    }

    public function fetchCredentials() {
        $response = $this->client->request('GET', 'credentials');

        return $response->getBody();
    }

    public function storeCredentials(string $alias, array $vc) {
        $response = $this->client->request('PUT', 'credentials/' . $alias, [
            'json' => $vc
        ]);

        return $response->getBody();
    }

    public function presentCredentials(string $holderDid, array $vcs) {
        $response = $this->client->request('POST', 'credentials/present', [
            'json' => [
                'holderDid' => $holderDid,
                'vcs' => $vcs
            ]
        ]);

        return $response->getBody();
    }

}
